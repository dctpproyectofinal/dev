﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace BusinessLogic
{
    public class UserCore
    {
        Login _login = Login.getuser();

        public UserCore() { }

        public bool autenticar(string _username, string _pass)
        {
            dbuser _dbuser = new dbuser();
            var query = _dbuser.daautenticar(_username, _pass);
            if (query.userid != null)
            {
                _login.UserID = query.userid;
                _login.UserName = query.username;
                return true;
            }
            return false;
        }
    }
}
