﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace BusinessLogic
{
    public class Login
    {
      
        private static Login _login;
        public string UserID { get; set; }
        public string UserName { get; set; }

        private Login() { }

        public static Login getuser()
        {
            if (_login == null)
            {
                _login = new Login();
            }
            return _login;
        }
    }
}
