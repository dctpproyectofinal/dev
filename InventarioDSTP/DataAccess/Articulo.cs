//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Articulo
    {
        public int ID { get; set; }
        public string Descripcion { get; set; }
        public Nullable<decimal> CostoUnitario { get; set; }
        public Nullable<decimal> PrecioUnitario { get; set; }
        public Nullable<bool> Estado { get; set; }
    }
}
