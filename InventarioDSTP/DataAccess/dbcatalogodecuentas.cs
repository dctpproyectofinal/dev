﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
   
    public class dbcatalogodecuentas
    {

        ContabilidadGeneralDSTPEntities db = new ContabilidadGeneralDSTPEntities();

        public List<tcatalogocuentas> GetCatalogo(){
            var query = from c in db.tcatalogocuentas select c;
            return query.ToList();
        }
    }
}
