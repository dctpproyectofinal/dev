﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
   
    public class dbarticulos
    {

        dbEntities db = new dbEntities();

        public List<Articulo> ListarArticulos(){
            var query = from c in db.Articulo select c;
            return query.ToList();
        }

        public void AgregarArticulo(string desc, decimal costo, decimal precio, bool estado) {
            Articulo articulo = new Articulo();
            articulo.Descripcion = desc;
            articulo.CostoUnitario = costo;
            articulo.PrecioUnitario = precio;
            articulo.Estado = estado;
            db.Articulo.Add(articulo);
            db.SaveChanges();
        }

        public void Actualizar(int id, string desc, decimal costo, decimal precio, bool estado) {
            var query = (from c in db.Articulo where c.ID == id select c).FirstOrDefault();
            query.Descripcion = desc;
            query.CostoUnitario = costo;
            query.PrecioUnitario = precio;
            query.Estado = estado;
            db.SaveChanges();
        }
    }
}
