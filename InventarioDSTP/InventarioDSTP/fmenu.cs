﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;

namespace InventarioDSTP
{
    public partial class fmenu : Form
    {
        Login _login = Login.getuser();
        public fmenu()
        {
            InitializeComponent();
        }

        private void fmenu_Load(object sender, EventArgs e)
        {
            //Login login;
            tsslusuario.Text = _login.UserName + " | ";
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tsslfecha.Text = DateTime.Now.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            farticulos farticulos = new farticulos();
            farticulos.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tbpContabilidad.SelectedTab = tbpContabilidad.TabPages["tabPage2"];
        }

        
    }
}
