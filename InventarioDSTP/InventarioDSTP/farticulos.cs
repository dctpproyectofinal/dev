﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioDSTP
{
    public partial class farticulos : Form
    {

        public farticulos()
        {
            InitializeComponent();
        }
        
        dbarticulos db = new dbarticulos();

        private void fcuentasprincipales_Load(object sender, EventArgs e)
        {
            Listar();
            //gvcatalogo
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            filtro();
        }

        public void filtro() {
            var query = (from c in db.ListarArticulos() where c.Descripcion.Contains(textBox1.Text) select c).ToList();
            if (query.Count() > 0)
            {
                gvarticulos.DataSource = query;
                gvarticulos.Refresh();
            }
            else
            {
                //gvcatalogo.DataSource = db.GetCatalogo();
                gvarticulos.Refresh();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tbid.Text == "")
            {
                AgregarArticulo();
            }
            else {
                ActualizarArticulo();
            }
        }

        private void ActualizarArticulo()
        {
            db.Actualizar(int.Parse(tbid.Text), tbdesc.Text, decimal.Parse(tbcosto.Text), decimal.Parse(tbprecio.Text), cbstado.Checked);
            Listar();
        }

        private void AgregarArticulo()
        {
            db.AgregarArticulo(tbdesc.Text, decimal.Parse(tbcosto.Text), decimal.Parse(tbprecio.Text), cbstado.Checked);
            Listar();
        }

        private void Listar() {
            gvarticulos.DataSource = db.ListarArticulos();
        }

        private void gvarticulos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            tbid.Text = Convert.ToString(gvarticulos.CurrentRow.Cells[0].Value);
            tbdesc.Text =  Convert.ToString(gvarticulos.CurrentRow.Cells[1].Value);
            tbcosto.Text = Convert.ToString(gvarticulos.CurrentRow.Cells[2].Value);
            tbprecio.Text = Convert.ToString(gvarticulos.CurrentRow.Cells[3].Value);
            cbstado.Checked = Convert.ToBoolean(gvarticulos.CurrentRow.Cells[4].Value);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            tbid.Text = "";
            tbdesc.Text = "";
            tbcosto.Text = "";
            tbprecio.Text = "";
            cbstado.Checked = false;
        }


    }
}
