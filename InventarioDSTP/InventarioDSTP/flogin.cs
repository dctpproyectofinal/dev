﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventarioDSTP
{
    

    public partial class flogin : Form
    {
        UserCore _usercore = new UserCore();

        public flogin()
        {
            InitializeComponent();
        }

        private void btniniciar_Click(object sender, EventArgs e)
        {
            fmenu _fmenu = new fmenu();
            string _user = tbuser.Text;
            string _pass = tbpass.Text;
            if (_usercore.autenticar(_user, _pass))
            {
                _fmenu.Show();
                this.Hide();      
            }
            else {
                lmessagge.Text = "El usuario y la contraseña que has introducido no coinciden.";
                lmessagge.Visible = true;
            }                
            //;
        }


    }
}
