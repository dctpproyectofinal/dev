USE [master]
GO
/****** Object:  Database [sistemafacturacion]    Script Date: 13/02/2016 17:12:53 ******/
CREATE DATABASE [sistemafacturacion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sistemafacturacion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\sistemafacturacion.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'sistemafacturacion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\sistemafacturacion_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [sistemafacturacion] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sistemafacturacion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sistemafacturacion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [sistemafacturacion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [sistemafacturacion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [sistemafacturacion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [sistemafacturacion] SET ARITHABORT OFF 
GO
ALTER DATABASE [sistemafacturacion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [sistemafacturacion] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [sistemafacturacion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [sistemafacturacion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [sistemafacturacion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [sistemafacturacion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [sistemafacturacion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [sistemafacturacion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [sistemafacturacion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [sistemafacturacion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [sistemafacturacion] SET  DISABLE_BROKER 
GO
ALTER DATABASE [sistemafacturacion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [sistemafacturacion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [sistemafacturacion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [sistemafacturacion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [sistemafacturacion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [sistemafacturacion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [sistemafacturacion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [sistemafacturacion] SET RECOVERY FULL 
GO
ALTER DATABASE [sistemafacturacion] SET  MULTI_USER 
GO
ALTER DATABASE [sistemafacturacion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [sistemafacturacion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [sistemafacturacion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [sistemafacturacion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'sistemafacturacion', N'ON'
GO
USE [sistemafacturacion]
GO
/****** Object:  Table [dbo].[Articulo]    Script Date: 13/02/2016 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripción] [varchar](100) NULL,
	[CostoUnitario] [money] NULL,
	[PrecioUnitario] [money] NULL,
	[Estado] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 13/02/2016 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[NoIdentidad] [varchar](50) NULL,
	[CuentaContable] [varchar](50) NULL,
	[Estado] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleFactura]    Script Date: 13/02/2016 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleFactura](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FacturaID] [int] NULL,
	[ArticuloID] [int] NULL,
	[Cantidad] [float] NULL,
	[PrecioUnitario] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Factura]    Script Date: 13/02/2016 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Factura](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CondicionPagoID] [int] NULL,
	[VendedorID] [int] NULL,
	[ClienteID] [int] NULL,
	[Fecha] [datetime] NULL,
	[Comentario] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vendedores]    Script Date: 13/02/2016 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendedores](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[PorcientoComision] [decimal](10, 2) NULL,
	[Estado] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [sistemafacturacion] SET  READ_WRITE 
GO
